package api.dao;

import impl.dao.RelDaoProduct;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import model.Product;

public abstract class DaoFactory {
    private static DaoFactory daoFactory;
    
    protected DaoFactory(){}
    
    public static DaoFactory getDaoFactory(){
        if(daoFactory == null){
            try(InputStream is = new BufferedInputStream(new FileInputStream("resources/storage_config.properties"))){
                Properties properties = new Properties();
                properties.load(is);
                daoFactory = new RelDaoFactory();
            }
            catch(FileNotFoundException e){
                e.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
        return daoFactory;
    }
    
    public abstract <K, E> Dao<K, E> getDao(Class<?> k, Class<?> e);
    
    
    private static class RelDaoFactory extends DaoFactory {
        private Connection connection;
        
        public RelDaoFactory(){
            this.connection = getConnection();
        }
        
        private Connection getConnection(){
            if(connection == null){
                try(InputStream is = new BufferedInputStream(new FileInputStream("resources/storage_config.properties"))){
                    Properties properties = new Properties();
                    properties.load(is);
                    connection = DriverManager.getConnection(properties.getProperty("url"),
                            properties.getProperty("login"), properties.getProperty("password"));
                    connection.setAutoCommit(false);
                }
                catch(SQLException e){
                    e.printStackTrace();
                }
                catch(FileNotFoundException e){
                    e.printStackTrace();
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }
            return connection;
        }
        
        @Override
        public <K, E> Dao<K, E> getDao(Class<?> k, Class<?> e){
            try{
                // reflection
                E entity = (E)e.newInstance();
                if(entity instanceof Product){
                    return (Dao<K, E>)(new RelDaoProduct(getConnection()));
                }
                return null;
            }
            catch(InstantiationException g){
                g.printStackTrace();
            }
            catch(IllegalAccessException g){
                g.printStackTrace();
            }
            return null;
        }
        
    }
}
