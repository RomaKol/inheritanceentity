package api.services;

import java.util.Collection;

public interface EntityService<K, E> {
    E create(E entity);
    E update(E entity);
    boolean delete(E entity);
    E findById(K key);
    Collection<E> findAll(int from, int to);
}
