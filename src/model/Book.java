package model;

public class Book extends Product {
    private long isbn;
    private String publisher;
    private int pages;
    private byte[] cover;
    
    public Book(String title, double price, String description, long isbn,
            String publisher, int pages, byte[] cover){
        super(title, price, description);
        this.isbn = isbn;
        this.publisher = publisher;
        this.pages = pages;
        this.cover = cover;
    }
    public Book(Long id, String title, double price, String description, long isbn,
            String publisher, int pages, byte[] cover){
        super(id, title, price, description);
        this.isbn = isbn;
        this.publisher = publisher;
        this.pages = pages;
        this.cover = cover;
    }

    public long getIsbn() {
        return isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public int getPages() {
        return pages;
    }

    public byte[] getCover() {
        return cover;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void setCover(byte[] cover) {
        this.cover = cover;
    }
    
}
