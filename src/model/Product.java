package model;

public class Product extends Entity <Long> {
    private String title;
    private double price;
    private String description;
    
    public Product(){}
    public Product(String title, double price, String description){
        this.title = title;
        this.price = price;
        this.description = description;
    }
    public Product(Long id, String title, double price, String description){
        super(id);
        this.title = title;
        this.price = price;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
