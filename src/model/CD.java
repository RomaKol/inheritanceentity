package model;

public class CD extends Product {
    private String musicCompany;
    private int duration;
    private String jenre;
    private int countTracks;
    
    public CD(String title, double price, String description, String musicCompany,
            int duration, String jenre, int countTracks){
        super(title, price, description);
        this.musicCompany = musicCompany;
        this.duration = duration;
        this.jenre = jenre;
        this.countTracks = countTracks;
    }
    public CD(Long id, String title, double price, String description, String musicCompany,
            int duration, String jenre, int countTracks){
        super(id, title, price, description);
        this.musicCompany = musicCompany;
        this.duration = duration;
        this.jenre = jenre;
        this.countTracks = countTracks;
    }

    public String getMusicCompany() {
        return musicCompany;
    }

    public int getDuration() {
        return duration;
    }

    public String getJenre() {
        return jenre;
    }

    public int getCountTracks() {
        return countTracks;
    }

    public void setMusicCompany(String musicCompany) {
        this.musicCompany = musicCompany;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setJenre(String jenre) {
        this.jenre = jenre;
    }

    public void setCountTracks(int countTracks) {
        this.countTracks = countTracks;
    }
    
}
