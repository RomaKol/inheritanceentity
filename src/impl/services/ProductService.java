package impl.services;

import api.dao.Dao;
import api.dao.DaoFactory;
import api.dao.RelDao;
import api.services.EntityService;
import impl.dao.RelDaoProduct;
import java.util.Collection;
import model.Book;
import model.CD;
import model.Product;

public class ProductService implements EntityService<Long, Product> {

    @Override
    public Product create(Product entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Product> dao = daoFactory.getDao(Long.class, Product.class);
        RelDao<Long, Product> relDao = (RelDao<Long, Product>)dao;
        RelDaoProduct relDaoProduct = (RelDaoProduct)relDao;
        
        Product product = (Product)(dao.create(entity));
        if(product == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        if(product instanceof Book){
            Book book = (Book)product;
            return book;
        }
        else if(product instanceof CD){
            CD cd = (CD)product;
            return cd;
        }
        return product;
    }

    @Override
    public Product update(Product entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Product> dao = daoFactory.getDao(Long.class, Product.class);
        RelDao<Long, Product> relDao = (RelDao<Long, Product>)dao;
        RelDaoProduct relDaoProduct = (RelDaoProduct)relDao;
        
        Product product = (Product)(dao.update(entity));
        if(product == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        if(product instanceof Book){
            Book book = (Book)product;
            return book;
        }
        else if(product instanceof CD){
            CD cd = (CD)product;
            return cd;
        }
        return product;
    }

    @Override
    public boolean delete(Product entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Product> dao = daoFactory.getDao(Long.class, Product.class);
        RelDao<Long, Product> relDao = (RelDao<Long, Product>)dao;
        RelDaoProduct relDaoProduct = (RelDaoProduct)relDao;
        
        boolean res = dao.delete(entity);
        if(!res){
            relDao.rollback();
        }
        else{
            relDao.commit();
        }
        return res;
    }

    @Override
    public Product findById(Long key) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Product> dao = daoFactory.getDao(Long.class, Product.class);
        RelDao<Long, Product> relDao = (RelDao<Long, Product>)dao;
        RelDaoProduct relDaoProduct = (RelDaoProduct)relDao;
        
        Product product = (Product)(dao.findById(key));
        if(product == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        if(product instanceof Book){
            Book book = (Book)product;
            return book;
        }
        else if(product instanceof CD){
            CD cd = (CD)product;
            return cd;
        }
        return product;
    }

    @Override
    public Collection<Product> findAll(int from, int to) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Product> dao = daoFactory.getDao(Long.class, Product.class);
        RelDao<Long, Product> relDao = (RelDao<Long, Product>)dao;
        RelDaoProduct relDaoProduct = (RelDaoProduct)relDao;
        
        Collection<Product> products = relDaoProduct.findAll(from, to);
        return products;
    }
    
}
