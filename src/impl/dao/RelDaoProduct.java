/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl.dao;

import api.dao.RelDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import model.Book;
import model.CD;
import model.Product;

/**
 *
 * @author raketa
 */
public class RelDaoProduct extends RelDao<Long, Product> {
    
    public RelDaoProduct(Connection connection){
        super(connection);
    }

    @Override
    public Product create(Product entity) {
        String sql = null;
        if(entity instanceof Book){
            sql = "INSERT INTO products (title, price, description, isbn,"
                    + "publisher, pages, cover, dtype) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
            Book book = (Book)entity;
            try(PreparedStatement psBook = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)){
                psBook.setString(1, book.getTitle());
                psBook.setDouble(2, book.getPrice());
                psBook.setString(3, book.getDescription());
                psBook.setLong(4, book.getIsbn());
                psBook.setString(5, book.getPublisher());
                psBook.setInt(6, book.getPages());
                psBook.setBytes(7, book.getCover());
                psBook.setString(8, "Book");
                
                int numbInsertedRows = psBook.executeUpdate();
                if(numbInsertedRows == 0){
                    return null;
                }
                ResultSet resultSet = psBook.getGeneratedKeys();
                Long bookId = null;
                while(resultSet.next()){
                    bookId = resultSet.getLong(1);
                }
                
                Book resBook = new Book(bookId, book.getTitle(), book.getPrice(), 
                        book.getDescription(), book.getIsbn(), book.getPublisher(), 
                        book.getPages(), book.getCover());
                
                return resBook;
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        else if(entity instanceof CD){
            sql = "INSERT INTO products (title, price, description, musicCompany,"
                    + "duration, jenre, countTracks, dtype) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
            CD cd = (CD)entity;
            try(PreparedStatement psCd = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)){
                psCd.setString(1, cd.getTitle());
                psCd.setDouble(2, cd.getPrice());
                psCd.setString(3, cd.getDescription());
                psCd.setString(4, cd.getMusicCompany());
                psCd.setInt(5, cd.getDuration());
                psCd.setString(6, cd.getJenre());
                psCd.setInt(7, cd.getCountTracks());
                psCd.setString(8, "CD");
                
                int numbInsertedRows = psCd.executeUpdate();
                if(numbInsertedRows == 0){
                    return null;
                }
                ResultSet resultSet = psCd.getGeneratedKeys();
                Long cdId = null;
                while(resultSet.next()){
                    cdId = resultSet.getLong(1);
                }
                
                CD resCd = new CD(cdId, cd.getTitle(), cd.getPrice(), 
                        cd.getDescription(), cd.getMusicCompany(), cd.getDuration(), 
                        cd.getJenre(), cd.getCountTracks());
                
                return resCd;
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Product update(Product entity) {
        String sql = null;
        if(entity instanceof Book){
            sql = "UPDATE products SET title=?, price=?, description=?, isbn=?, "
                    + "publisher=?, pages=?, cover=? WHERE id = ?";
            Book book = (Book)entity;
            try(PreparedStatement psBook = connection.prepareStatement(sql)){
                psBook.setString(1, book.getTitle());
                psBook.setDouble(2, book.getPrice());
                psBook.setString(3, book.getDescription());
                psBook.setLong(4, book.getIsbn());
                psBook.setString(5, book.getPublisher());
                psBook.setInt(6, book.getPages());
                psBook.setBytes(7, book.getCover());
                psBook.setLong(8, book.getId());
                
                int numbUpdatedRows = psBook.executeUpdate();
                if(numbUpdatedRows == 0){
                    return null;
                }
                
                return book;
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        else if(entity instanceof CD){
            sql = "UPDATE products SET title=?, price=?, description=?, musicCompany=?, "
                    + "duration=?, jenre=?, countTracks=? WHERE id = ?";
            CD cd = (CD)entity;
            try(PreparedStatement psCd = connection.prepareStatement(sql)){
                psCd.setString(1, cd.getTitle());
                psCd.setDouble(2, cd.getPrice());
                psCd.setString(3, cd.getDescription());
                psCd.setString(4, cd.getMusicCompany());
                psCd.setInt(5, cd.getDuration());
                psCd.setString(6, cd.getJenre());
                psCd.setInt(7, cd.getCountTracks());
                psCd.setLong(8, cd.getId());
                
                int numbUpdatedRows = psCd.executeUpdate();
                if(numbUpdatedRows == 0){
                    return null;
                }
                
                return cd;
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public boolean delete(Product entity) {
        String sql = "DELETE FROM products WHERE id = ?";
        try(PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1, entity.getId());
            int numberDeletedRows = ps.executeUpdate();
            if(numberDeletedRows == 0){
                return false;
            }
            return true;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Product findById(Long key) {
        String sql = "SELECT p.id, p.title, p.price, p.description, p.isbn, p.publisher, "
                + "p.pages, p.cover, p.musicCompany, p.duration, p.jenre, p.countTracks, p.dtype "
                + "FROM products as p WHERE id = ?";
        try(PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1, key);
            
            ResultSet resultSet = ps.executeQuery();
            while(resultSet.next()){
                long id = resultSet.getLong(1);
                String title = resultSet.getString(2);
                double price = resultSet.getDouble(3);
                String desciption = resultSet.getString(4);
                long isbn = resultSet.getLong(5);
                String publisher = resultSet.getString(6);
                int pages = resultSet.getInt(7);
                byte[] cover = resultSet.getBytes(8);
                String musicCompany = resultSet.getString(9);
                int duration = resultSet.getInt(10);
                String jenre = resultSet.getString(11);
                int countTracks = resultSet.getInt(12);
                String className = resultSet.getString(13);
                
                if(className == "Book"){
                    Book book  = new Book(id, title, price, desciption, isbn, 
                            publisher, pages, cover);
                    return book;
                }
                else if(className == "CD"){
                    CD cd = new CD(id, title, price, desciption, musicCompany, 
                            duration, jenre, countTracks);
                    return cd;
                }
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Collection<Product> findAll(int from, int to) {
        ArrayList<Product> products = new ArrayList();
        String sql = "SELECT p.id, p.title, p.price, p.description, p.isbn, p.publisher, "
                + "p.pages, p.cover, p.musicCompany, p.duration, p.jenre, p.countTracks, p.dtype "
                + "FROM products as p LIMIT ?, ?";
        try(PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1, from);
            ps.setLong(2, to);
            
            ResultSet resultSet = ps.executeQuery();
            while(resultSet.next()){
                long id = resultSet.getLong(1);
                String title = resultSet.getString(2);
                double price = resultSet.getDouble(3);
                String desciption = resultSet.getString(4);
                long isbn = resultSet.getLong(5);
                String publisher = resultSet.getString(6);
                int pages = resultSet.getInt(7);
                byte[] cover = resultSet.getBytes(8);
                String musicCompany = resultSet.getString(9);
                int duration = resultSet.getInt(10);
                String jenre = resultSet.getString(11);
                int countTracks = resultSet.getInt(12);
                String className = resultSet.getString(13);
                
                if(className.equals("Book")){
                    Book book  = new Book(id, title, price, desciption, isbn, 
                            publisher, pages, cover);
                    products.add(book);
                }
                else if(className.equals("CD")){
                    CD cd = new CD(id, title, price, desciption, musicCompany, 
                            duration, jenre, countTracks);
                    products.add(cd);
                }
            }
            
            return products;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }
    
}
